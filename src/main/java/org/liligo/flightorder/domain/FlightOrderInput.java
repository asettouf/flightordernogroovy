package org.liligo.flightorder.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.OffsetDateTime;

@Getter
@Setter
public class FlightOrderInput {

    private OffsetDateTime inboundArrivalDate;
    private OffsetDateTime inboundDepartureDate;
    private OffsetDateTime outboundDepartureDate;
    private OffsetDateTime outboundArrivalDate;
    private int price;
    private CommonFlightInfo commonFlightInfo;

    public FlightOrder toFlightOrder(){
        FlightOrder flightOrder = new FlightOrder();
        flightOrder.setCommonFlightInfo(this.commonFlightInfo);
        flightOrder.setFlightDuration((int) Duration.between(this.outboundDepartureDate.toInstant(), this
                .outboundArrivalDate.toInstant()).toMinutes());
        flightOrder.setPricePerPassenger((double) (this.price) / this.commonFlightInfo.getNumberOfPassenger());
        flightOrder.setTripStart(this.outboundDepartureDate);
        flightOrder.setTripEnd( this.inboundDepartureDate != null ? this.inboundDepartureDate
                : this.outboundArrivalDate);
        flightOrder.setTripType(this.inboundArrivalDate != null ? TripType.ROUND_TRIP : TripType.ONE_WAY);
        return flightOrder;
    }

}
