package org.liligo.flightorder.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.liligo.flightorder.deserializers.FlightOrderSerializer;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@JsonSerialize(using = FlightOrderSerializer.class)
public class FlightOrder {
    @GeneratedValue
    @Id
    @JsonIgnore
    private long id;
    @OneToOne(cascade = CascadeType.ALL)
    private CommonFlightInfo commonFlightInfo;
    private TripType tripType;
    private OffsetDateTime tripEnd;
    private OffsetDateTime tripStart;
    private double pricePerPassenger;
    private int flightDuration;

}
