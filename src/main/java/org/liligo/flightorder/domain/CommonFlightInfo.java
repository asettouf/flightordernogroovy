package org.liligo.flightorder.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class CommonFlightInfo {
    @GeneratedValue
    @Id
    @JsonIgnore
    private long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Location locationFrom;
    @OneToOne(cascade = CascadeType.ALL)
    private Location locationTo;
    private int numberOfPassenger;
    private String airline;
}
