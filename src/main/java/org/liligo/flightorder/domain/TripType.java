package org.liligo.flightorder.domain;

public enum TripType {
    ROUND_TRIP, ONE_WAY;
}
