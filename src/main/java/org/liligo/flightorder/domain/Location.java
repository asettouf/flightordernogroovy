package org.liligo.flightorder.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Location {
    @GeneratedValue
    @Id
    @JsonIgnore
    private long id;
    private String country;
    private String city;
}
