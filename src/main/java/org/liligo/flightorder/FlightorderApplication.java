package org.liligo.flightorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages =  "org.liligo.flightorder")
public class FlightorderApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightorderApplication.class, args);
	}

}
