package org.liligo.flightorder.controller;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.baeldung.jpa.dao")
public class PersistenceConfig {
}
