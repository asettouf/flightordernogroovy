package org.liligo.flightorder.controller;

import org.liligo.flightorder.domain.FlightOrder;
import org.liligo.flightorder.service.FlightOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FlightOrderController {

    @Autowired
    private FlightOrderService flightOrderService;

    @PostMapping(value = "/flightorders", consumes = "application/json", produces = "application/json")
    public void injectFlightOrders(@RequestBody String json){
        flightOrderService.save(json);
    }

    @GetMapping("/flightorders")
    public List<FlightOrder> findAll(){
        return flightOrderService.findAll();
    }

    @GetMapping("/flightordersByMaxPrice")
    public List<FlightOrder> findByPrice(@RequestParam double price){
        return flightOrderService.findByMaxPrice(price);
    }
}
