package org.liligo.flightorder.deserializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.liligo.flightorder.domain.FlightOrder;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class FlightOrderSerializer extends StdSerializer<FlightOrder> {
    public FlightOrderSerializer() {
        this(null);
    }
    protected FlightOrderSerializer(Class<FlightOrder> vc) {
        super(vc);
    }


    @Override
    public void serialize(FlightOrder flightOrder, JsonGenerator jgen, SerializerProvider serializerProvider) throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("provider", flightOrder.getCommonFlightInfo().getAirline());
        jgen.writeStringField("inbound", flightOrder.getTripEnd().atZoneSameInstant(ZoneOffset.UTC).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        jgen.writeStringField("outbound",
                flightOrder.getTripStart().atZoneSameInstant(ZoneOffset.UTC).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        jgen.writeStringField("tripType", flightOrder.getTripType().name());
        jgen.writeNumberField("outboundFlightDuration", flightOrder.getFlightDuration());
        jgen.writeNumberField("pricePerPassenger", flightOrder.getPricePerPassenger());
        jgen.writeObjectField("from", flightOrder.getCommonFlightInfo().getLocationFrom());
        jgen.writeObjectField("to", flightOrder.getCommonFlightInfo().getLocationTo());
        jgen.writeEndObject();
    }
}
