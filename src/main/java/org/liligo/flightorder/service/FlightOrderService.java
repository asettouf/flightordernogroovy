package org.liligo.flightorder.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.liligo.flightorder.domain.*;
import org.liligo.flightorder.repository.FlightOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class FlightOrderService {

    @Autowired
    private FlightOrderRepository flightOrderRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public void save(String json) {
        List<FlightOrder> flightOrderInputs = null;
        try {
            flightOrderInputs = parseJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveFlightOrders(flightOrderInputs);
    }

    public List<FlightOrder> findAll(){
        List<FlightOrder> result = new ArrayList<>();
        flightOrderRepository.findAll().forEach(result::add);
        return result;
    }

    public List<FlightOrder> findByMaxPrice(double price){
        List<FlightOrder> result = new ArrayList<>();
        flightOrderRepository.findByPricePerPassengerLessThan(price).forEach(result::add);
        return result;
    }

    private List<FlightOrder> parseJson(String json) throws IOException {
        JsonNode jsonElems = objectMapper.readTree(json);
        List<FlightOrder> flightOrders = new ArrayList<>();
        for (Iterator<JsonNode> it = jsonElems.elements(); it.hasNext(); ) {
            JsonNode jsonNode = it.next();
            flightOrders.add(parseOneElem(jsonNode).toFlightOrder());
        }
        return flightOrders;
    }

    private FlightOrderInput parseOneElem(JsonNode jsonNode) {
        FlightOrderInput flightOrderInput = new FlightOrderInput();
        Location locationTo = new Location();
        locationTo.setCity(jsonNode.get("to").get("city").asText());
        locationTo.setCountry(jsonNode.get("to").get("country").asText());
        Location locationFrom = new Location();
        locationFrom.setCity(jsonNode.get("from").get("city").asText());
        locationFrom.setCountry(jsonNode.get("from").get("country").asText());
        CommonFlightInfo commonFlightInfo = new CommonFlightInfo();
        commonFlightInfo.setLocationFrom(locationFrom);
        commonFlightInfo.setLocationTo(locationTo);
        commonFlightInfo.setAirline(jsonNode.get("provider").asText());
        commonFlightInfo.setNumberOfPassenger(jsonNode.get("numberOfPassengers").asInt());
        flightOrderInput.setCommonFlightInfo(commonFlightInfo);
        if (jsonNode.hasNonNull("inbound")) {
            flightOrderInput.setInboundArrivalDate(OffsetDateTime.parse(jsonNode.get("inbound").get("arrival").asText()));
            flightOrderInput.setInboundDepartureDate(OffsetDateTime.parse(jsonNode.get("inbound").get("departure").asText()));
        }
        flightOrderInput.setOutboundArrivalDate(OffsetDateTime.parse(jsonNode.get("outbound").get("arrival").asText()));
        flightOrderInput.setOutboundDepartureDate(OffsetDateTime.parse(jsonNode.get("outbound").get("departure").asText()));
        flightOrderInput.setPrice(jsonNode.get("price").asInt());
        return flightOrderInput;
    }

    private void saveFlightOrders(List<FlightOrder> flightOrders) {
        for (FlightOrder f : flightOrders) {
            saveFlightOrder(f);
        }

    }

    private void saveFlightOrder(FlightOrder flightOrder) {
        if (validateFlightOrder(flightOrder)) {
            flightOrderRepository.save(flightOrder);
        }
    }

    private boolean validateFlightOrder(FlightOrder flightOrder) {
        return (!flightOrder.getCommonFlightInfo().getLocationFrom().getCountry().equals(flightOrder.getCommonFlightInfo().getLocationTo().getCountry())
                && (flightOrder.getTripStart().compareTo(flightOrder.getTripEnd()) < 0) && flightOrder.getPricePerPassenger() < 1000. &&
                flightOrder.getTripType() == TripType.ROUND_TRIP && (ChronoUnit.DAYS.between(flightOrder.getTripStart().toInstant(),
                flightOrder.getTripEnd().toInstant()) > 7) && flightOrder.getFlightDuration() > 0);


    }

}

