package org.liligo.flightorder.repository;

import org.liligo.flightorder.domain.FlightOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FlightOrderRepository extends CrudRepository<FlightOrder, Long> {
    List<FlightOrder> findByPricePerPassengerLessThan(double price);
}
